"""
    NFC Target definition
"""

import nfc

def get_nt():
    """
        Create new nfc target object.
    """

    nt = nfc.target()
    nt.nm.nmt = nfc.NMT_ISO14443A # 14443A
    nt.nm.nbr = nfc.NBR_UNDEFINED # Variable bitrate
    nt.nti.nai.abtAtqa = bytearray([0x00, 0x04]) # ATQA
    nt.nti.nai.abtUid = bytearray([0x08, 0xab, 0xcd, 0xef]) # UID
    nt.nti.nai.btSak = 0x32 # SAK
    nt.nti.nai.szUidLen = 4
    ATS = bytearray(b"\x78\x00\x70\x03") # ATS
    nt.nti.nai.abtAts = ATS
    nt.nti.nai.szAtsLen = len(ATS)
    return nt
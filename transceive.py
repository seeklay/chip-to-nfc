"""
    Chip to NFC
"""

from smartcard.System import readers
from hexdump import hexdump
from nt import get_nt
import enum
import nfc

class NFCBitrate(enum.Enum):

    VARIABLE = 0
    A106 = 1

class NFCError(enum.Enum):

    INIT_TIMEOUT = -6
    RF_Transmission_Error = -20
    Target_Released = -10

readers = readers()
if len(readers) < 1:
    raise RuntimeError("There are no PC/SC readers!")
reader = readers[0]
print(f"PC/SC device: {reader}")

context = nfc.init()
dev = nfc.open(context)

if dev is None:
    nfc.exit(context)
    raise RuntimeError("Failed to open NFC device!")
print(f"NFC device: {nfc.device_get_name(dev)}")

print("Starting...")
while 7:
    print()
    target = get_nt()
    print("Waiting for initiation request...")
    ireq = nfc.target_init(dev, target, 2, 000)
    if ireq[0] < 0:
        nfc.perror(dev, "target_init")
        print(f"Communication error during initing [{ireq[0]}]")
        continue

    print(f"Received target ireq, bitrate: {NFCBitrate(target.nm.nbr)}")
    conn = reader.createConnection()
    conn.connect()
    print(f"Opened PC/SC connection with protocol: {conn.getProtocol()}")
    #print(f"Card ATR: {conn.getATR()}")
    print("Starting transceive: \n")

    while 7:
        print()
        request = nfc.target_receive_bytes(dev, 264, 0)
        if request[0] < 0:
            #nfc.perror(dev, "target_receive_bytes")
            print(f"Communication error during receiving [{NFCError(request[0])}]")
            conn.disconnect()
            break

        data = request[1][:request[0]]
        print(hexdump(data))

        data = [i for i in data]
        response, w1, w2 = conn.transmit(data)

        if w1 == 0x61:
            # if smartcard respond with "Response bytes available", retrieve {w2} of available bytes
            response, w1, w2 = conn.transmit([i for i in bytes.fromhex(f"A0 c0 00 00 {hex(w2)[2:].zfill(2)}")])

        elif w1 == 0x6c:
            # if smartcard respond with "Bad length value in Le; ‘xx’ is the correct exact Le", edit command and try again
            response, w1, w2 = conn.transmit([*data[:-1], w2])

        response.extend([w1, w2])  # add status words to response

        if response == [0x6a, 0x82]:
            # if the card doesn't have a PPSE, respond with a user-defined PPSE
            if data == [i for i in bytes.fromhex("00 a4 04 00 0e 32 50 41 59 2e 53 59 53 2e 44 44 46 30 31 00")]:
                response = bytearray.fromhex('6F23840E325041592E5359532E4444463031A511BF0C0E610C4F07A00000065810108701019000')
                print("SPOOF PPSE [2PAY.SYS] with MIR AID")

        response = bytearray(response)
        print('---------------------------------')
        print(hexdump(response))

        s = nfc.target_send_bytes(dev, response, len(response), 0)
        print(f"Respond. Sent: {s}/{len(response)} [{response.hex()}]")

print("Exiting...")
nfc.close(dev)
nfc.exit(context)

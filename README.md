# chip to nfc
## about
connect emulated nfc (ISO14443A-4) smartcard with physical contact (ISO7816) smartcard

## requirements 
* libnfc >= 1.7.1
* python3
* [nfc-bindings](https://github.com/xantares/nfc-bindings)
* [pyscard](https://pyscard.sourceforge.io/)
* nfc-reader (like pn532)
* smartcard-reader (may need to install libccid)

## transceive procedure
1. loop: create nfc emulating target
2. init nfc device as target
3. wait for initiation request
4. open smartcard reader connection
5. loop: wait for command from nfc initiator
6. send this command to smartcard
7. send response to nfc initiator
8. wait for nfc error/communication end
9. -loop: close smartcard connection
10. to-loop: wait for new initiation request

## more
i tested this script on nfc emv reader software for android, and it looks really good
idk if it works on a real terminal but it looks like it can
it is also possible to make a radio relay out of it

## tests video
soon
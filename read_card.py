"""
    SmartCard hex terminal
"""

from smartcard.Exceptions import NoCardException
from smartcard.System import readers
from smartcard.util import toHexString
import smartcard
import sys
from hexdump import hexdump

reader = readers()[0]
conn = reader.createConnection()
try:
    conn.connect()
except NoCardException:
    exit("Insert card first!")
except smartcard.Exceptions.CardConnectionException as e:
    exit(e)

print(f"Connected via: {conn.getReader()}  Card ATR: {toHexString(conn.getATR())}")

def c(hex: str):
    return [i for i in bytes.fromhex(hex)]

while 7:
    try:
        apdu = [i for i in bytes.fromhex(input())]
        rsp, sw1, sw2 = conn.transmit(apdu)
        if sw1 == 0x61:
            rsp, sw1, sw2 = conn.transmit(c(f"A0 c0 00 00 {hex(sw2)[2:]}"))
        if sw1 == 0x6c:
            rsp, sw1, sw2 = conn.transmit([*apdu[:-1], sw2])
        rsp.extend([sw1, sw2])
        print(hexdump(rsp))
    except KeyboardInterrupt:                    
        conn.disconnect()
        break
